# Start service locally
* Clone project 
* Install requirements 
* Run flask service ($ export FLASK_APP=app.py / $ export FLASK_ENV=development / $ flask run)

# Call service
* URL: https://protandem.herokuapp.com/

# Used technologies
* Python, Flask, Gitlab, Heroku

# Developement Operations (DevOps) and CI/CD
![](https://assets4.botmetric.com/wp-content/uploads/2017/11/Ingraphics-devops-to-agile.png)

![](https://devopedia.org/images/article/54/7602.1513404277.png)

![](https://www.xenonstack.com/images/insights/2020/01/continuous-integration-and-continuous-delivery-xenonstack.png)

# Flask as a microframework
* The “micro” in microframework means Flask aims to keep the core simple but extensible.
* Flask won’t make many decisions for you, such as what database to use.
* Those decisions that it does make, such as what templating engine to use, are easy to change.
* Everything else is up to you, so that Flask can be everything you need and nothing you don’t.
* By default, Flask does not include a database abstraction layer, form validation or anything else where different libraries already exist that can handle that.
* Instead, Flask supports extensions to add such functionality to your application as if it was implemented in Flask itself. Numerous extensions provide database integration, form validation, upload handling, various open authentication technologies, and more. Flask may be “micro”, but it’s ready for production use on a variety of needs.

# Flask-RESTX for building REST-APIs with Swagger-Support 
* Flask-RESTX is an extension for Flask that adds support for quickly building REST APIs. 
* Flask-RESTX encourages best practices with minimal setup. If you are familiar with Flask, 
* Flask-RESTX should be easy to pick up. It provides a coherent collection of decorators and tools to describe your API and expose its documentation properly (using Swagger).
* Flask-RESTX is a community driven fork of Flask-RESTPlus

[Flask-RESTX](https://flask-restx.readthedocs.io/en/latest/)

# Gunicorn
The Web Server Gateway Interface (pronounced whiskey or WIZ-ghee) is a simple calling convention for web servers to forward requests to web applications or frameworks written in the Python programming language.

[Gunicorn](https://gunicorn.org/)
[Web Server Gateway Interface](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface)

[Flask introduction](https://flask.palletsprojects.com/en/1.1.x/foreword/)

# What is Gitlab
* a web-based DevOps (Developement Operations) lifecycle tool
* provides a Git-repository manager
* providing wiki
* issue-tracking
* continuous integration and deployment pipeline features
* using an open-source license which covers the core functionality

# Heroku as PaaS
Platform-as-a-service (PaaS) is a form of cloud computing where hardware and an application software platform is provided by another party. Primarily for developers and programmers, a PaaS allows the user to develop, run, and manage their own apps without having to build and maintain the infrastructure or platform usually associated with the process.

 ![alt text](https://www.redhat.com/cms/managed-files/iaas-paas-saas-diagram3-1638x1046.png)
* see also here: [Cloud computing](https://www.redhat.com/en/topics/cloud-computing/what-is-paas)

# Implementation sources links
[Deploying a Flask app to Heroku](https://stackabuse.com/deploying-a-flask-application-to-heroku/)

[Setting up CI/CD on Gitlab for deploying a Python Flask app to Heroku](https://www.freecodecamp.org/news/setting-up-a-ci-cd-on-gitlab-for-deploying-a-python-flask-application-on-heroku-e154db93952b/)

# app.py
from flask import Flask, request, jsonify, render_template, make_response
from flask_restx import Api, Resource, reqparse, fields
app = Flask(__name__)
api = Api(app, version='1.0', title='Message API',
    description='A simple message API',)

ns = api.namespace('messages', description='Messages')
html = api.namespace('html_examples', description='html examples')

## Default namespace example ##
@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        return "hello world!"

## Defined namespace example ##
@html.route('/html')
class HelloWorld(Resource):
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('index.html'), 200, headers)

## Defined namespace example ##
@ns.route('/message')
class SimpleMessages(Resource):
    @ns.param('name', "A name")
    def get(self):
        # Retrieve the name from url parameter
        name = request.args.get("name", None)

        # For debugging
        print(f"got name the perfect {name}")

        response = {}

        # Check if user sent a name at all
        if not name:
            response["ERROR"] = "no name found, please send a name."
        # Check if the user entered a number not a name
        elif str(name).isdigit():
            response["ERROR"] = "name can't be numeric."
        # Now the user entered a valid name
        else:
            response["MESSAGE"] = f"Welcome {name} to this awesome holiday platform!! " \
                                  f"I am looking forward booking a beautiful time out:-)"

        # Return the response in json format
        return jsonify(response)

    @ns.param('name', "A name")
    def post(self):
        name = request.args.get('name')
        print(name)
        if name:
            return jsonify({
                "Message": f"Welcome {name} to our awesome platform!!",
                # Add this option to distinct the POST request
                "METHOD" : "POST"
            })
        else:
            return jsonify({
                "ERROR": "no name found, please send a name."
            })

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000)
